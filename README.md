# Certification Examples


In this repo you find some examples of Jupyter Reports. The documentation of Jupyter Reports is available
[online](https://hardt-solutions.gitlab.io/jupyter-reports/).


## Folder "Quick Start"

The example notebook of the Jupyter Reports documentation is placed this folder.
You can use it to get a general understanding of how Jupyter Reports works.

For the export of the notebook to PDF, call in the command line

    jr-build-pdfs -f ipynb -o output_pdf -s "How to write reports with Jupyter Reports" -e "AFBS.certification_template.exporter.AFBSLatexExporter"

The exported notebook as PDF is also in the folder.


## Folder "STC Example"

In this folder are some notebook of the MASB-STC of the Akaflieg Braunschweig. This should serve "real world example".

The documents are:

- AFBS-S01-PCP-010-001: "Zertifizierungsprogramm"
- AFBS-S01-CAR-010-100: "Nachweisrechnung - Alle Muster"
- AFBS-S01-CAR-010-001: "Erweitern der Nachweisrechnung für den Duo Discus T"
- AFBS-S01-FMS-043-001: "Anhang zum Flughandbuch - Duo Discus (T)"

The data flow between the notebooks is as follows:

- AFBS-S01-PCP-010-001 -> AFBS-S01-CAR-010-100
- AFBS-S01-PCP-010-001 -> AFBS-S01-CAR-010-001
- AFBS-S01-CAR-010-001 -> AFBS-S01-FMS-043-001


For the export of the notebook to PDF, call in the command line

    jr-build-pdfs -f ipynb -o output_pdf -s "AFBS-S01-*" -e "AFBS.certification_template.exporter.AFBSLatexExporter"

The exported notebooks as PDF are also in the folder.


## Further Questions?

Contact me:

    Daniel Hardt
    daniel@daniel-hardt.de
    +49 160 3417342
