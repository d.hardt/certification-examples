{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to write reports with Jupyter Reports\n",
    "\n",
    "This notebook shows a few best practices for writing reports with Jupyter Reports. Jupyter Reports is based on Jupyter Notebooks (https://jupyter.org/) so working with Jupyter Notebooks is implied.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General\n",
    "\n",
    "The report is written in the Jupyter Notebook using code cells (and the output of the code cells) and Markdown cells (https://en.wikipedia.org/wiki/Markdown). Writing content is explained in [this section](#Writing-report-content). These code cell outputs and Markdown content is later exported to LaTeX and then compiled to a PDF file (described in [this section](#Export))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing report content\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "\n",
    "Please make sure that you clear the cell outputs (Edit > Clear All Outputs) before committing changes of the notebook in git, otherwise tacking of changes become difficult and the repo become very big.\n",
    "\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Imports\n",
    "\n",
    "All important functions and classes for writing the reports are in the `jupyter_reports.notebooks` module. The `jupyter_reports.dependencies` module provides functions for the export from objects from a notebook and the import of these objects (see [this section](#Dependencies))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import\n",
    "from jupyter_reports.notebooks import *\n",
    "from jupyter_reports.dependencies import *\n",
    "\n",
    "# Get data path\n",
    "import os\n",
    "data_path = os.path.join('.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all, the language has to be set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_language('en') # 'en' for English, 'de' for German"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After this, the name of the current notebook and it path must be given to Jupyter Reports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_notebook_name_and_path('How to write reports with Jupyter Reports', os.path.abspath(''))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `RunMode` determines the formatting of the output and, for example, the checking of factors of safety. It can be set using the following method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "set_run_mode(RunMode.NORMAL)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For normal editing `RunMode.NORMAL` should be used, for debugging `RunMode.DEBUG`. If the notebook should be exported to PDF, the run mode must be set to `RunMode.PUBLISH_TEST` or `RunMode.PUBLISH`. With `RunMode.PUBLISH` the export aborts if a required factors of safety is not achieved, `RunMode.PUBLISH_TEST` ignores this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Text\n",
    "\n",
    "Regular text can be easily written into Markdown cells or displayed from code using the `md` function. Headings are created with a leading hash '#'. With more than one hash mark, the heading can be indented further in the structure. When generating from the code, variable values can also be included in the output.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "\n",
    "Per Markdown cell max one heading is allowed and must be uppermost in the cell.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_variable = 15.4321\n",
    "md(f'Test value: {test_variable:.2f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The multi-line strings in Python are suitable for longer texts with included variables: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "md(f'''\n",
    "Multiline String:\n",
    "Test value 4e: {test_variable:.4e}'\n",
    "Test value 2f: {test_variable:.2f}'\n",
    "''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "LaTeX commands can also be included in the Markdown text as usual, inline With dollar signs (i.e. $\\tau_t^2$) or as detached eqation in the 'equation' environement:\n",
    "\n",
    "\\begin{equation}\n",
    "c^2 = a^2 + b^2\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Images\n",
    "\n",
    "Images can not be included directly in Markdown if the notebook is exported later. Use the `img` function instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img('sb10.jpg', # File\n",
    "    'SB 10 - biggest glider in the world.', # Caption\n",
    "    'fig:sb10', # LaTeX label\n",
    "    width=0.5) # relative width (fraction of the page width)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Figures\n",
    "\n",
    "Figures can be created with `matplotlib` and are also displayed with the `img` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate test data\n",
    "x = np.linspace(0, 5, 50)\n",
    "y1 = np.sin(x)\n",
    "y2 = np.cos(x)\n",
    "\n",
    "# Create plot\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(x, y1, label='sin')\n",
    "ax.plot(x, y2, label='cos')\n",
    "ax.set_xlabel('x [rad]')\n",
    "ax.set_ylabel('y [-]')\n",
    "ax.legend()\n",
    "\n",
    "# Show figure\n",
    "img(fig,\n",
    "    'Sinus and Cosinus', # Caption\n",
    "    'fig:sin-cos') # LaTeX label"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Equations\n",
    "\n",
    "With the `sympy` package and the extensions in `jupyter_reports.notebooks`, equations can be created, displayed, filled with values and calculated very conveniently. Variables are objects of the `jupyter_reports.notebooks.ValueSymbol` class and have a symbol, a unit and, if available, a value. Furthermore, a different unit and a formatting string can be specified for the output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma_max = ValueSymbol(\n",
    "    r'\\sigma_{max}',  # LaTeX symbol name\n",
    "    200e6 * units.Pa,  # Value and unit\n",
    "    description='Max Stress',  # Optional: Description of the symbol\n",
    "    format_string='{:.0f}',  # Optional: Format string used to format the value every time it is displayed\n",
    "    output_unit=units.MPa  # Optional: If the value of the symbol is displayed, it is converted to this unit.\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default `format_string` can be set using the `set_default_format_string` function. This will set the `format_string` to this value for all subsequently created symbols. The variables can be output using various methods: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "md(f'Only value: {sigma_max.value_str()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "md(f'Value with unit: {sigma_max.value_unit_str()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "md(f'Symbol with value and unit: {sigma_max.symbol_value_unit_str()}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "md(f'''\n",
    "And the whole thing again\n",
    "in several lines and the output \n",
    "{sigma_max.symbol_value_unit_str()}\n",
    "integrated into the text.\n",
    "''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or as a numbered LaTeX equation (free-standing, not integrated into the text): "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ltx(sigma_max.symbol_value_unit_str())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Equations can be easily written in Python using the symbol variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining symbols\n",
    "force = ValueSymbol('F', 2e3 * units.N, format_string='{:.1f}')\n",
    "area = ValueSymbol('A', 2 * units.cm**2, format_string='{:.3f}', output_unit=units.mm**2)\n",
    "pressure = ValueSymbol('p', units.Pa, output_unit=units.MPa)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining the equation\n",
    "pressure_equation = Eq(pressure, force / area)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be noted that the result symbol must be defined with the unit before the calculation, so that a unit check is carried out during the calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display equation wthout evaluation\n",
    "display(pressure_equation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `evaluate_and_display_equation` function is used to evaluate equations and to display the results in a comprehensible manner:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate and display equation\n",
    "evaluate_and_display_equation(pressure_equation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save time the `evaluate_and_display_equation` function can be used with resulting symbol an expression directly, without defining the equation before with `Eq`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate and display equation (alternative)\n",
    "evaluate_and_display_equation(pressure, force / area)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result of the equation can be compared with a given factor of safety:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate factor of safety\n",
    "max_pressure = ValueSymbol('p_{max}', 20 * units.MPa)\n",
    "factor_of_safety = ValueSymbol('j', units.One) # Existing factor of safety\n",
    "factor_of_safety_prescribed = ValueSymbol('j_{pre}', 1.5) # Prescribed factor of safety\n",
    "\n",
    "evaluate_and_display_equation(factor_of_safety, max_pressure / pressure,\n",
    "                              required_factor_of_safety=factor_of_safety_prescribed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mathematical functions\n",
    "\n",
    "The matematical functions (e.g. `sqrt`, `sin`, `cos`) of the `sympy` package must be used. The standard Python functions or `numpy` functions do not work with the `sympy` symbols."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining symbols\n",
    "x = ValueSymbol('x', 2 * units.m, format_string='{:.1f}')\n",
    "alpha = ValueSymbol(r'\\alpha', 30 * units.deg, format_string='{:.1f}')\n",
    "y = ValueSymbol('y', units.m)\n",
    "\n",
    "# Evaluate and display equation\n",
    "evaluate_and_display_equation(y, x * sym.sin(alpha))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tables\n",
    "\n",
    "Tables must be `pandas.DataFrame` objects. Strings can be used for column labeling. However, it is recommended to use objects of the `jupyter_reports.notebooks.LabelSymbol` class objects for column labeling, as this class provides extended formatting options and can also be used for calculations with table columns, similar to the `evaluate_and_display_equation` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining LabelSymbols\n",
    "force_label = LabelSymbol('F', units.N, format_string='{:.1f}')\n",
    "area_label = LabelSymbol('A', units.cm**2, output_unit=units.mm**2)\n",
    "pressure_label = LabelSymbol('p', units.Pa, output_unit=units.MPa)\n",
    "\n",
    "# Create table\n",
    "table = pd.DataFrame({force_label: np.linspace(0, 1e3, 11), area_label: np.linspace(1, 2, 11)})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tables are displayed using the `tab` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tab(table,\n",
    "    'Test table', # Caption\n",
    "    'tab:test') # LaTeX label"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tables can also be easily filtered by rows and columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Filter columns\n",
    "table_columns_filtered = table[[area_label]]\n",
    "\n",
    "# Filter rows\n",
    "table_row_filtered = table[table[force_label] < 500]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display table\n",
    "tab(table_columns_filtered,\n",
    "    'Test table columns filtered.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display table\n",
    "tab(table_row_filtered,\n",
    "    'Test table rows filtered.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find more information about filtering in the pandas documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When using `LabelSymbol`s as column labels, the columns can be used in equations like normal symbols. Similar to the `evaluate_and_display_equation` function, the calculation can be performed with the `evaluate_and_display_table_equation` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining the table equation\n",
    "pressure_table_equation = Eq(pressure_label, force_label / area_label)\n",
    "\n",
    "# Evaluate and display table equation\n",
    "evaluate_and_display_table_equation(table, pressure_table_equation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display table\n",
    "tab(table,\n",
    "    'Test table with new calculated column.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save time the `evaluate_and_display_table_equation` function can be used with resulting label an expression directly, without defining the equation before with `Eq`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate and display table equation\n",
    "evaluate_and_display_table_equation(table, pressure_label, force_label / area_label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The a column of the table can be compared with a given factor of safety:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate factor of safety\n",
    "max_pressure_label = ValueSymbol('p_{max,2}', 20 * units.MPa)\n",
    "factor_of_safety_label = LabelSymbol('j_{2}', units.One) # Existing factor of safety\n",
    "\n",
    "evaluate_and_display_table_equation(table, factor_of_safety_label, max_pressure_label / pressure_label)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display table\n",
    "tab(table,\n",
    "    'Test table with factor of safety.',\n",
    "    required_factor_of_safety={factor_of_safety_label: factor_of_safety_prescribed})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Citing\n",
    "\n",
    "For citing, simply use the LaTeX `\\cite` command (e.g. \\cite{cs}). The biblatex library will later be used for citing in LaTeX.\n",
    "\n",
    "Example: \\cite{cs}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### References to equations, figures and tables\n",
    "\n",
    "References can be made with the LaTeX `\\ref` command (e.g. \"see figure \\ref{fig:sin-cos}\"), or with the cleverref package and the `\\cref` command (e.g. \"see \\cref{fig:sin-cos}\"). The labels must not contain any special character (ä, ö, ü, ...)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dependencies\n",
    "\n",
    "With Jupyter Reports it is easy to reuse objects (symbols, tables, figures, ...) from other notebooks. These objects are saved with the pickle module (https://docs.python.org/3/library/pickle.html) in a .p-file. This pickle file can later be read by another notebook and thus the objects of the previous notebook can be reused. To simplify this process there are the `export_workspace` and `import_workspace` functions.\n",
    "\n",
    "The `export_workspace` function exports the objects from the current notebook. You can also select which objects are to be exported.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Export all relevant objects\n",
    "export_workspace()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `import_workspace` function imports objects from another notebook. If the notebook to be imported has not yet exported any objects to a pickle file, this notebook will be executed out automatically. This resolves the dependencies between the notebooks without the user having to run all notebooks one after the other after making changes in one notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import der allgemeinen Daten\n",
    "import_workspace('previous_notebook.ipynb')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display equation from \"previous_notebook.ipynb\"\n",
    "eqn(equation_from_previous_notebook)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Export\n",
    "\n",
    "The Jupyter Report notebooks are converted to PDF-files using `nbconvert` (https://nbconvert.readthedocs.io/). Templates are used to allow the users to adapt the output to their needs. These templates need additional information, e.g. the report title, author, date.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook metadata\n",
    "\n",
    "The metadata information is added to the notebook metadata (in JupyterLab: on the right side click on \"Property Inspector\" > \"Advanced Tools\" > \"Notebook Metadata\") in the JSON format. The template determines which information must be or can be given. One example of these metadata for the default template is given below:\n",
    "\n",
    "\n",
    "```json\n",
    "\"document_metadata\": {\n",
    "    \"author\": {\n",
    "        \"email\": \"email@example.com\",\n",
    "        \"name\": \"Max Mustermann\",\n",
    "        \"phone\": \"+12 345 6789\"\n",
    "    },\n",
    "    \"doc_type\": \"Document Type\",\n",
    "    \"document_id\": \"ABC-DEF-123-456\",\n",
    "    \"issue_date\": \"2022-07-24\",\n",
    "    \"issue_num\": \"03\",\n",
    "    \"revision_history\": [\n",
    "        {\n",
    "            \"affected_sections\": \"all\",\n",
    "            \"author\": \"Max Mustermann\",\n",
    "            \"change_summary\": \"Issue of the document\",\n",
    "            \"date\": \"2022-07-22\",\n",
    "            \"revision\": \"01\"\n",
    "        },\n",
    "        {\n",
    "            \"affected_sections\": \"Chapter 3\",\n",
    "            \"author\": \"Max Mustermann\",\n",
    "            \"change_summary\": \"Change of XY\",\n",
    "            \"date\": \"2022-07-24\",\n",
    "            \"revision\": \"02\"\n",
    "        }\n",
    "    ],\n",
    "    \"title\": \"Document Title\"\n",
    "},\n",
    "\"latex_metadata\": {\n",
    "    \"bibliography\": \"literatur.bib\"\n",
    "},\n",
    "\"nbconvert_metadata\": {\n",
    "    \"display_stream\": false\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Export of the notebook to PDF\n",
    "\n",
    "There are two possibilities to perform th export of the notebook to a PDF-file.\n",
    "\n",
    "#### Export via command line (recommended)\n",
    "\n",
    "The recommended way to export the notebooks is the `jr-build-pdfs` command of the Jupyter Reports package. Therefore open the command line, navigate to the directory of the notebook to export and execute the following command:\n",
    "\n",
    "```bash\n",
    "jr-build-pdfs -f ipynb -o output_dir\n",
    "```\n",
    "\n",
    "With this command, all notebook in the direcory and all subdirectories are executed and exported to PDF. The results are written in the directory given by the `-o` argiment (`output_dir`). To select one or more notebooks to exprt, use the `-s`-argument. It expect a serach string (can contain wildcards) and every matching notebook is selected for the export:\n",
    "\n",
    "```bash\n",
    "jr-build-pdfs -f ipynb -o output_pdf -s \"*_final\"\n",
    "```\n",
    "\n",
    "\n",
    "#### Export via JupyterLab (not recommended)\n",
    "\n",
    "Open the notebook in JupyterLab and run all cells. Click on \"File\" > \"Save and Export Notebook As...\" > \"Jupyter_reports_pdf\". If there are no errors in the notebook, the PDF will be provided as download.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "Note\n",
    "\n",
    "Citing does not work with this export option.\n",
    "\n",
    "</div>\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "document_metadata": {
   "author": {
    "email": "email@example.com",
    "name": "Max Mustermann",
    "phone": "+12 345 6789"
   },
   "doc_type": "Document Type",
   "document_id": "ABC-DEF-123-456",
   "issue_date": "2022-07-24",
   "issue_num": "03",
   "revision_history": [
    {
     "affected_sections": "all",
     "author": "Max Mustermann",
     "change_summary": "Issue of the document",
     "date": "2022-07-22",
     "revision": "01"
    },
    {
     "affected_sections": "Chapter 3",
     "author": "Max Mustermann",
     "change_summary": "Change of XY",
     "date": "2022-07-24",
     "revision": "02"
    }
   ],
   "title": "Document Title"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "latex_metadata": {
   "bibliography": "literatur.bib"
  },
  "nbconvert_metadata": {
   "display_stream": false
  },
  "toc-autonumbering": true,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false,
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
